Changelog
=========

## Unreleased
[Full Changelog](https://github.com/pcfens/puppet-static_custom_facts/compare/v0.1.0...HEAD)

## [v0.1.1](https://github.com/pcfens/puppet-static_custom_facts/tree/v0.1.0)
[Full Changelog](https://github.com/pcfens/puppet-static_custom_facts/compare/v0.1.0...v0.1.1)

- Fix formatting of README (no changes in puppet or testing)

## [v0.1.0](https://github.com/pcfens/puppet-static_custom_facts/tree/v0.1.0)

- Initial Release
